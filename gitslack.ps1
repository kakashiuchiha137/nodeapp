$SlackWebhookURI = 'https://hooks.slack.com/services/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'
Function Send-Notification($changes) {
    #Send-SlackMessage -Uri $SlackWebhookURI -Text "Changes detected in the main branch with following files: $changes"

    # Your Slack webhook URL
    $WebhookURL = $SlackWebhookURI

# Sample list of files
    $FilesList = $changes

# Constructing the message with bullet points
$Message = @"
Here are the commit changes in the main branch:

$($FilesList | ForEach-Object { "$_" } | Out-String)
"@

# Sending the message to Slack
Invoke-RestMethod -Uri $WebhookURL -Method Post -Body (@{ text = $Message } | ConvertTo-Json) -ContentType "application/json"
}

#Send-Notification($changedFiles)
# Define your Git repository path
$repoPath = "C:\nodeapp"

# Change directory to the repository path
Set-Location -Path $repoPath

# Function to check if there are any changes in the Git repository
function CheckGitChanges {
    # Fetch the changes from the remote repository
    git fetch

    # Compare the local and remote branches
    $localBranch = git rev-parse --abbrev-ref HEAD
    $remoteBranch = "origin/$localBranch"

    $localCommit = git rev-parse $localBranch
    $remoteCommit = git rev-parse $remoteBranch

    # Check if there are changes between the local and remote branches
    if ($localCommit -ne $remoteCommit) {
        # Get the list of changed files using git diff
        $changedFiles = git diff --name-only $localCommit $remoteCommit
        return $changedFiles
    } else {
        return $null
    }
}

# Function to perform git pull
function GitPull {
    git pull

    # Check if the pull was successful
    if ($LASTEXITCODE -eq 0) {
        Write-Host "Git pull successful."
    } else {
        Write-Host "Error occurred during git pull."
    }
}

# Main script logic
try {
    # Check if git is installed and accessible
    $gitExecutable = Get-Command git -ErrorAction Stop

    # Check if the repository directory exists
    if (Test-Path $repoPath) {
        Set-Location -Path $repoPath

        # Check for changes in the remote repository and perform a pull if needed
        $changedFiles = CheckGitChanges
        if ($changedFiles) {
            # Store the list of changed files in the $changedFiles variable
            $filesCommaSeparated = $changedFiles -join ","
            $filesNewLineSeparated = $changedFiles -join "`n"
            Send-Notification($filesNewLineSeparated)
            Write-Host "Changed files (comma-separated): $filesCommaSeparated"
            Write-Host "Changed files (new line-separated):"
            Write-Host $filesNewLineSeparated

            GitPull
        } else {
            Write-Host "No need to pull changes."
        }
    } else {
        Write-Host "Git repository directory not found at $repoPath."
    }
} catch {
    Write-Host "An error occurred: $_"
}

